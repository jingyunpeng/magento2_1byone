var config = {
    paths: {
        'owlcarousel': "TouchShop_Basic/js/owl.carousel.min"
    },
    shim: {
        'owlcarousel': {
            deps: ['jquery']
        }
    }
};